/**programming III Lab2
 * class Bicycle, object of a bicycle
 * field manufacturer, number of gears, and max speed
 * author: Shirley Maa ID:2037242
 * version: 09/03/2021
 */

 public class Bicycle{
     private String manufacturer;
     private int numberGears;
     private double maxSpeed;

    /*c. construtor allow a programmer to create a Bicycle and 
    initialize the fields to whatever values they want*/
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer=manufacturer;
        this.numberGears=numberGears;
        this.maxSpeed=maxSpeed;
    }
    //b. get method for 3 fields
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
        return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }
    
    /*toString method :should return a String representation of the Bicycle
    * object. For example “Manufacturer: Specialized, Number of Gears: 21, MaxSpeed: 40)
    */
    public String toString(){
        return "Manufacture: " +this.manufacturer+", Number of Gears: " +this.numberGears+ ", MaxSpeed: " +this.maxSpeed;
    } 

 }
