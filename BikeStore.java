/**programming III Lab2
 * class BikeStore : list of bicycle
 * with class Bicycle: field of manufacturer, number of gears, and max speed
 * author: Shirley Maa ID:2037242
 * version: 09/03/2021
 */
public class BikeStore{
    public static void main(String[] args){
        Bicycle[] bikes=new Bicycle[4];
        bikes[0]=new Bicycle("Supercycle", 18, 40);
        bikes[1]=new Bicycle("Giant", 20, 60);
        bikes[2]=new Bicycle("Cannondale", 21, 60);
        bikes[3]=new Bicycle("CCM", 21, 50);

        //print list of bikes
        for(Bicycle list: bikes){
            System.out.println(list);
        }
       /*2nd try print list of bikes 
       for(int i=0; i<bikes.length; i++){
        System.out.println(bikes[i]);
        }
        */
    }
}